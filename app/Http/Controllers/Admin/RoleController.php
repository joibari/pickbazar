<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index(){
        return view('admin.role.index');
    }
    public function permission(){
        $role = Role::latest()->get();
        return view('admin.role.permission' , compact('role'));
    }
    public function getRole(){
        return Role::latest()->get();
    }
    public function edit(Role $role){
        return $role;
    }
    public function update(Role $role , Request $request){
        $tax = Role::where('name' , $request->name)->first();
        if ($tax){
            return 'exist';
        }
        else{
            $role->update([
                'name'=> $request->name,
                'updated_at'=> Carbon::now(),
            ]);;
        }
    }
    public function remove(Role $role){
        $role->delete();
        return 'success';
    }
    public function changePermission(Request $request){
        $role = Role::where('id' , $request->id)->update([
            'permission'=>$request->permission,
        ]);
        return 'success';
    }
    public function store(Request $request){
        $tax = Role::where('name' , $request->name)->first();
        if ($tax){
            return 'exist';
        }
        else{
            return $tax = Role::create([
                'name'=>$request->name,
            ]);
        }
    }
}
