<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(){
        return view('admin.user.index');
    }
    public function edit(User $user){
        $role = $user->role;
        return [$user , $role];
    }
    public function getUser(){
       return User::latest()->get();
    }
    public function store(Request $request){
        $user = User::create([
            'name'=> $request->name,
            'email'=> $request->email,
            'number'=> $request->number,
            'password'=> Hash::make($request->password),
            'profile'=> $request->profile,
        ]);
        $user->role()->sync($request->level);
        return 'success';
    }
    public function update(User $user, Request $request){
        if ($request->password){
            $user->update([
                'name'=> $request->name,
                'email'=> $request->email,
                'number'=> $request->number,
                'password'=> Hash::make($request->password),
                'profile'=> $request->profile,
                'updated_at'=> Carbon::now(),
            ]);
        }else{
            $user->update([
                'name'=> $request->name,
                'email'=> $request->email,
                'number'=> $request->number,
                'profile'=> $request->profile,
                'updated_at'=> Carbon::now(),
            ]);
        }
        $user->role()->detach();
        $user->role()->sync($request->level);
        return 'success';
    }
    public function remove(User $user){
        $user->role()->detach();
        $user->delete();
        return 'success';
    }
    public function login(){
        return view('admin.user.login');
    }
}
