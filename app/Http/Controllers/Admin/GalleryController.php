<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function store(Request $request){
        $file = $request->file;
        $name = $file->getClientOriginalName();
        $type = $file->getClientOriginalExtension();
        $sizefile = $file->getsize()/1000;
        if( $sizefile > 1000){
            $size=round($sizefile/1000 ,2) . 'mb';
        }else{
            $size=round($sizefile) . 'kb';
        }
        if ($type == "jpg" or $type == "png" or $type == "jpeg" or $type == "svg" or $type == "tif" or $type == "gif" or $type == "jfif"){
            $url = "/upload/image";
            $path = $file->move(public_path($url) , $name);
            Gallery::create([
                'name' => $name,
                'size' => $size,
                'type' => $type,
                'url' => $url . '/' . $name ,
                'path' => $path->getRealPath(),
            ]);
        }
        elseif ($type == "rar" or $type == "zip"){
            $url = "/upload/file";
            $path = $file->move(storage_path($url) , $name);
            Gallery::create([
                'name' => $name,
                'size' => $size,
                'type' => $type,
                'url' => $url . '/' . $name ,
                'path' => $path->getRealPath(),
            ]);
        }
        elseif ($type == "mp3"){
            $url = "/upload/music";
            $path = $file->move(storage_path($url) , $name);
            Gallery::create([
                'name' => $name,
                'size' => $size,
                'type' => $type,
                'url' => $url . '/' . $name ,
                'path' => $path->getRealPath(),
            ]);
        }
        elseif ($type == "mp4" or $type == "mkv"){
            $url = "/upload/movie";
            $path = $file->move(storage_path($url) , $name);
            Gallery::create([
                'name' => $name,
                'size' => $size,
                'type' => $type,
                'url' => $url . '/' . $name ,
                'path' => $path->getRealPath(),
            ]);
        }else{
            return "error";
        }
        return "success";
    }
    public function getGallery(){
        return Gallery::latest()->whereIn('type', ['gif','jpeg','jpg','png','svg','tif','jfif'])->first();
    }
}
