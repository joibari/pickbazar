<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function searchUser(Request $request){
       return $search1 = User::where("name" , "LIKE" , "%{$request->search}%")->get();
    }
    public function searchRole(Request $request){
       return $search1 = Role::where("name" , "LIKE" , "%{$request->search}%")->get();
    }
}
