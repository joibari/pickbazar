/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import EventHub from 'vue-event-hub';
Vue.use(EventHub);
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('SvgContent', require('./components/Admin/Svg/SvgContent.vue').default);
Vue.component('SvgIcon', require('./components/Admin/Svg/SvgIcon.vue').default);
Vue.component('AllPanel', require('./components/Admin/AllPanel.vue').default);
Vue.component('CreatePost', require('./components/Admin/Post/CreatePost.vue').default);
Vue.component('TablePanel', require('./components/Admin/Table/TablePanel.vue').default);
Vue.component('OrderTable', require('./components/Admin/Table/OrderTable.vue').default);
Vue.component('CouponTable', require('./components/Admin/Table/CouponTable.vue').default);
Vue.component('CreateOrder', require('./components/Admin/Order/CreateOrder.vue').default);
Vue.component('UserPanel', require('./components/Admin/User/UserPanel.vue').default);
Vue.component('RoleIndex', require('./components/Admin/Role/RoleIndex.vue').default);
Vue.component('PermissionPanel', require('./components/Admin/Role/PermissionPanel.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
