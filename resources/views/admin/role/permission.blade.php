@extends('admin.master')
@section('content')
    <permission-panel :role="{{json_encode($role)}}"></permission-panel>
@endsection
