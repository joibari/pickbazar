@extends('admin.master')
@section('content')
    <div class="allProductCreate">
        <div class="allProductCreateTitle">
            <span>Product</span>
            <category-panel></category-panel>
            <input type="text" placeholder="Ex: Search By Name">
        </div>
        <div class="allProductCreateItems">
            <ul>
                <li>
                    <div class="allProductCreateItemsPic">
                        <img src="https://s3.amazonaws.com/redqteam.com/pickbazar/kitchen_accessories_solid_turner.jpg" alt="">
                        <div class="off">
                            <span>20% Off</span>
                        </div>
                    </div>
                    <div class="allProductCreateItemsName">
                        Solid Turner
                    </div>
                    <h4>1 pc(s)</h4>
                    <div class="price">
                        <h5>$3</h5>
                        <s>$99</s>
                    </div>
                </li>
                <li>
                    <div class="allProductCreateItemsPic">
                        <img src="https://s3.amazonaws.com/redqteam.com/pickbazar/neutrogena_Body_Oil.jpg" alt="">
                    </div>
                    <div class="allProductCreateItemsName">
                        Solid Turner
                    </div>
                    <h4>1 pc(s)</h4>
                    <div class="price">
                        <h5>$2</h5>
                    </div>
                </li>
                <li>
                    <div class="allProductCreateItemsPic">
                        <img src="https://s3.amazonaws.com/redqteam.com/pickbazar/doritos.jpg" alt="">
                        <div class="off">
                            <span>30% Off</span>
                        </div>
                    </div>
                    <div class="allProductCreateItemsName">
                        Doritos Tangy Cheese
                    </div>
                    <h4>1 pc(s)</h4>
                    <div class="price">
                        <h5>$3</h5>
                        <s>$99</s>
                    </div>
                </li>
                <li>
                    <div class="allProductCreateItemsPic">
                        <img src="https://s3.amazonaws.com/redqteam.com/pickbazar/Narrow_Notch.jpg" alt="">
                    </div>
                    <div class="allProductCreateItemsName">
                        Solid Turner
                    </div>
                    <h4>1 pc(s)</h4>
                    <div class="price">
                        <h5>$1</h5>
                        <s>80</s>
                    </div>
                </li>
                <li>
                    <div class="allProductCreateItemsPic">
                        <img src="https://s3.amazonaws.com/redqteam.com/pickbazar/laundry_products_ecos.jpg" alt="">
                        <div class="off">
                            <span>20% Off</span>
                        </div>
                    </div>
                    <div class="allProductCreateItemsName">
                        Solid Turner
                    </div>
                    <h4>1 pc(s)</h4>
                    <div class="price">
                        <h5>$3</h5>
                        <s>$99</s>
                    </div>
                </li>
                <li>
                    <div class="allProductCreateItemsPic">
                        <img src="https://s3.amazonaws.com/redqteam.com/pickbazar/badia_pinenuts.jpg" alt="">
                        <div class="off">
                            <span>20% Off</span>
                        </div>
                    </div>
                    <div class="allProductCreateItemsName">
                        Solid Turner
                    </div>
                    <h4>1 pc(s)</h4>
                    <div class="price">
                        <h5>$3</h5>
                        <s>$99</s>
                    </div>
                </li>
                <li>
                    <div class="allProductCreateItemsPic">
                        <img src="https://s3.amazonaws.com/redqteam.com/pickbazar/oil_goya.jpg" alt="">
                        <div class="off">
                            <span>20% Off</span>
                        </div>
                    </div>
                    <div class="allProductCreateItemsName">
                        Solid Turner
                    </div>
                    <h4>1 pc(s)</h4>
                    <div class="price">
                        <h5>$3</h5>
                        <s>$99</s>
                    </div>
                </li>
                <li>
                    <div class="allProductCreateItemsPic">
                        <img src="https://s3.amazonaws.com/redqteam.com/pickbazar/Brook_taverner_scapoli_ladies_waistcoat.jpg" alt="">
                        <div class="off">
                            <span>20% Off</span>
                        </div>
                    </div>
                    <div class="allProductCreateItemsName">
                        Solid Turner
                    </div>
                    <h4>1 pc(s)</h4>
                    <div class="price">
                        <h5>$3</h5>
                        <s>$99</s>
                    </div>
                </li>
            </ul>
            <div class="more">
                <span>Load More</span>
            </div>
        </div>
    </div>
@endsection
