@extends('admin.master')
@section('content')
    <div class="allCouponCreate">
        <div class="allCouponCreateTitle">
            <span>Coupon</span>
            <category-panel></category-panel>
            <input type="text" placeholder="Ex: Search By Name">
            <button>
                <i>
                    +
                </i>
                Add Coupon
            </button>
        </div>
        <coupon-table></coupon-table>
    </div>
@endsection
