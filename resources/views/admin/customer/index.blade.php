@extends('admin.master')
@section('content')
    <div class="allCustomerCreate">
        <div class="allCustomerCreateTitle">
            <span>Customers</span>
            <input type="text" placeholder="Ex: Search By Name">
            <category-panel></category-panel>
        </div>
        <customer-table></customer-table>
    </div>
@endsection
