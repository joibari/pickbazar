<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/admin.css">
</head>
<body>
<div id="app">
    <div class="allPanel">
        <svg-content></svg-content>
        <div class="allHeader">
            <div class="allHeaderRight">
                <div class="headerLogo">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfUAAABRCAMAAAD8bM9vAAAAV1BMVEVHcEwAxY0AxY0AxY0AxY0AxY0WH2oAxY0WH2oWH2oWH2oAxY0WH2oWH2oWH2oWH2oAxY0AxY0AxY0WH2oAxY0AxY0WH2oWH2oAxY0WH2oWH2oWH2oAxY3irKvCAAAAG3RSTlMA0xHuRHd3u7tE7mafETPMmYgzIqoiZt1ViFU6cENSAAAI50lEQVR42u2dyaKqMAxAQco8ySza///Ot3h3UEiTdEDxSrZCAhxpkzQpnmcv12K63W63ZO68Qz5E0un2I3N5PI+PkPF2L/WB/fOgH9g/QrrbUorjofx5KVbUb4dL9+dlDf0WOVKdLBWnx+Peif8OUE8O6n9crgD1+qC+f6lSTIiTI4B6tgvqraDko6knN0KSIkrfb4SPJS1B3OTDQV0pWTEyAzd3odv21KWUUvrNcFBXg49K3tnpe1GXUsbioI5w57hzifd21KXsTwd1pdQVnaap3pG6DC4HdbWspveyJg54D+pS5gd1DezefPfrVHnvSv2zsGtSB7B3RfY1AYybXtjG1KU4qKslBVM90ZiWG1/Y1tT900Fd7cqXL7qwranL8KCuluKvUg9OB3W1VH+U+ge97AbUi79K3f9c6kX0LUUywdgtymWq9Ev5mHZPpX4O7+Tsw9ixIf4kvk4WT5wINjA6CJETD7ccJ7xcZk4Wggz/6Zws1nSiyoJ6mQBSlArqi7hsCAOd4E00/sOKTfv9Qx4vpPn+pYnZ4s4oaLj9/RPlfSCljOmHO6OLqew38ieqXyR5o9KUeoL5HDEJdPC5E/spXB8af6Vww9UPBnOMO6PorQ/n4PtwGhuAXZt6VWgu5tGaIzSHRFP32oBHHRwVpIyHLakbGsVuPbw7nIFtQjI1LOplgWcARgPqKe5nMqivHx5EfYjRHO421I2Nqm+99aUe9RGZ2DnU04yMCkpd6mWGl/FwqA8M6pcAoXXeiLq5UeWtP6hkUS/tqM+MYBBYtsE1Q5P6fd8Nh7pHU89xXOdNqFsYVd16vjic87LWy2NmDeoFL89baVGPKBUs6jFFPSfzOhtQtzGquPVcGlBP1EMpeXrBTe9XGtRTclmIRd0nDsppYL1z6lZG4VsX8snUC3bGb7mqg2gGJ/XR06ZOZGnawCC/Z0vdziho+OI/mfqoU5/Fpp7QaWIO9RbPyJ58+XzqlkZBw740oj6ZenNdpkF90SGn1hwxqvA51M/4tN7IF1C3NMozzKLeGfvwmks7HYt6BcUApT51ga+0DvIF1G2NOqS+freuPOqjHvTHUVqluZw4EQBNfR0Uh/ardrbU491QB3ynjkd9sqnOUmmG/MOrp0l9yGNinVXIp1EP3Bl1R73AGhix00d4uSX1PK+KauplV2iOeC3za5fmbhEK9JiCFp/0v47qQyEuYU8DgPsrobRb7s6oK+pdgsLBTp/QlPs1w2d2WHPFrPLQHyof+yBOcNlF/rtmGTAArKXHFvrcGLWnfgXj7SuLekXMv92EvrWgZmhSr13U0gScBM3DxH86G1APwby6W6NG1OvfEgVF3DXxouqZcrqqDCMIai4Y7rsJdX+g38nlHwOGhFLP8aotN0aRW/8pwjComxt51Cdy/o1umn5ixHHf9an7OZ22g5pjGk3qQN7toQjfjVHFrZ/ve/X1qU+8XEqFn6iIDkZMc8Vy33Wp90CvE+BMNzwrCPVTgLuQjoyCt94PljWyVx71keFqX6OlYPUa5cTf8UqDOrRvQcgrlm91qEPJVrGB0ZhR861NfWbmTQvL0tq15kKjSFtvXl/tW9AzS+p6Depnavx2ZDRmDBm61BdbhqqpJ5abU61LtsmLsfDhmxNxOlyZfOFTx913h0ap/JMB9Zq9Hjrhg4SL9gyk5U47Xvdb1JlTNUiwqV+AuZbSZWY0ZriEiRV0hLrtppP0hWWVy96XB8eK3QwVM6lT7rtDo5whQ4v6uqpRg3rqmvrouaQugwEBoNrToOFRBzy5YCBfYTOjnD+iBnWoglljPdQ19Uir48l/7AcBB3kkhhLc2Rqm7lNpf4dGY0b4x6c+s7YeS5WlbY6p4z2W5EqrOCPxjWvqpPu+KfXQmPoUdXodKptTj+yoP/YFLAZdx9RzyZmyN6N+MaNezxU/qn4WdbyNnlNBtZ5sz5tQF2TMti11wXi49WOPaBSlermUp3lzWWdJfZ3l+smFsR0rBnWO++7SqBH11I7NEyM37BszLOrr+TZ3H7kB7rti36OtIrdnUs9cZ2kyHYeOR/2iGnnZyZeAPLBnuO9Ojb6UemK5w+zq/FEHO4+6UAVv3ORoS5JqqKod/YxsK/dLfZ02Ly01XzUSNTzqnqpclbsQcqao55I9W7sy+lLqEQPR+kMUaFXFfGO7iDzq67emVS2VgHPxEBDUW5777tToS6lXN3qIn/T6K8qanYs3nNe/DxOsCBsqeYq1/hVUhGdi9KXU1+7cqu5l1K2ggirtSgvqjTJEBhK2LSf78gCA7747NPpi6gW1MgrUxkyE5uh24zmJLOpASdOgfqFWrregChfPbPfdodEXU79SEXaiXxntcetpWNR7dfNRCC3KXagyiUcAIefVpTTqGn01dWCIv/+aRFebdEFwa+cYtz70yLIb3JBwV3MoYqohQcd9d2b09dTBvcGK/wdUc2bW8ZTy6mQZa25QH0mPLZNJKWUcitYTIvTpjqeAVfFKrc3pGd0B9VLRRVEntUV3I68mnlhfV20IEeLzp06jocaeE6EzozugDr/siCQszQmn/8WwJXiw1hAjDjlJfZtO5idTLzVbmXm7FkAbYNSlE+ox5SxvTV38AerwXlHWO5RcGZU1ZtQFkUHfnPomO5Q8mzprh0H93YhmurQmtn54UDS/PXVLo/ug7tX8WsyOrbms9Xce0yySNR5uLalbGt0J9ZKLXWuXwYpUYEI952RNlqGBa+p2RndCnYvdxY6inR31nB0/30XgoXPqVkb3Qt0rEyPohOaCqKhy9O1GggC2ka8xdRuju6HOCduTTldzSaT2bHcoYY238WkT6hZGd0TdS/FRPosMNKd4l7Um9Ua5ACrUW3w2aLmqBXVzo3ui7nkjkq8pOiPNml8AQcfMAVsSUYTQvvA2o25sdF/UPW+E3/es6Ew111gmn0+9z6mPZg3AROuHJ29D6qZG90bd87poefBUXC00g1sTf7uFHOpx3IS8DzEvP7j0u5XNZtTNjGoUij5RqjEq/jfVRFHaeW8kJxGe/2/bFYq3N/oPtbqmx8W3q0QAAAAASUVORK5CYII=" alt="">
                </div>
            </div>
            <div class="allHeaderLeft">
                <div class="allHeaderLeftButton">
                    <create-post></create-post>
                </div>
                <div class="allHeaderLeftAlert">
                    <i>
                        <svg-icon :icon="'#alert'"></svg-icon>
                    </i>
                    <div class="alertDetail">
                        <div class="alertTitle">
                            <div class="alertTitleRight">
                                <span>Clear all</span>
                            </div>
                            <div class="alertTitleLeft">
                                <span>Notification</span>
                            </div>
                        </div>
                        <div class="alertSubject">
                            <div class="alertSubjectTime">
                                <h4>5m</h4>
                                <h4>=</h4>
                                <span>Delivery Successful</span>
                            </div>
                            <div class="alertSubjectCode">
                                <span>Order #34567 had been placed</span>
                            </div>
                        </div>
                        <div class="alertDetailMore">
                            <span>more feeds</span>
                        </div>
                    </div>
                </div>
                <div class="allHeaderLeftUser">
                    <div class="allHeaderLeftUserLogo">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/4gKgSUNDX1BST0ZJTEUAAQEAAAKQbGNtcwQwAABtbnRyUkdCIFhZWiAH3QAMAAQAAQAQAC5hY3NwQVBQTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9tYAAQAAAADTLWxjbXMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAtkZXNjAAABCAAAADhjcHJ0AAABQAAAAE53dHB0AAABkAAAABRjaGFkAAABpAAAACxyWFlaAAAB0AAAABRiWFlaAAAB5AAAABRnWFlaAAAB+AAAABRyVFJDAAACDAAAACBnVFJDAAACLAAAACBiVFJDAAACTAAAACBjaHJtAAACbAAAACRtbHVjAAAAAAAAAAEAAAAMZW5VUwAAABwAAAAcAHMAUgBHAEIAIABiAHUAaQBsAHQALQBpAG4AAG1sdWMAAAAAAAAAAQAAAAxlblVTAAAAMgAAABwATgBvACAAYwBvAHAAeQByAGkAZwBoAHQALAAgAHUAcwBlACAAZgByAGUAZQBsAHkAAAAAWFlaIAAAAAAAAPbWAAEAAAAA0y1zZjMyAAAAAAABDEoAAAXj///zKgAAB5sAAP2H///7ov///aMAAAPYAADAlFhZWiAAAAAAAABvlAAAOO4AAAOQWFlaIAAAAAAAACSdAAAPgwAAtr5YWVogAAAAAAAAYqUAALeQAAAY3nBhcmEAAAAAAAMAAAACZmYAAPKnAAANWQAAE9AAAApbcGFyYQAAAAAAAwAAAAJmZgAA8qcAAA1ZAAAT0AAACltwYXJhAAAAAAADAAAAAmZmAADypwAADVkAABPQAAAKW2Nocm0AAAAAAAMAAAAAo9cAAFR7AABMzQAAmZoAACZmAAAPXP/bAEMABQMEBAQDBQQEBAUFBQYHDAgHBwcHDwsLCQwRDxISEQ8RERMWHBcTFBoVEREYIRgaHR0fHx8TFyIkIh4kHB4fHv/bAEMBBQUFBwYHDggIDh4UERQeHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHv/AABEIAIAAgAMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAFBgMEBwgCAQD/xAA7EAACAQMDAwEFBgQFBAMAAAABAgMABBEFBiESMUFRBxMiYXEUMoGRobEVI0LBFiQzctElQ2LwNFLh/8QAGgEAAgMBAQAAAAAAAAAAAAAABAUBAgMABv/EACcRAAICAQQCAgICAwAAAAAAAAECABEDBBIhMRNBIlEFgWGhFLHB/9oADAMBAAIRAxEAPwDoTe+l232yKeOLpkb77DzQ/TNDTUD1NIUReD0jk0ybt5KL6Amhuk3ItLGVyfp9aSk0TUaAWojRpttFa2kcEJIVBgZq4oOOBn8KRIdavFcssnGe1M8FpbapaxzX8Cz5GQr5Kj8O1EIa4mLD3CvI7gj6ikfee4dv6deHGo9OoqOkrbnLD/cew/Gkb237t0rblrJp+3pJdNuw6pc3NtKyqiH7yhM9LNjjkcZrGNK1SCeAyWc7LGxDySySFmP/AJHJOTkefWrXL48W4XNk1P2qM8ptYyJpIyQeuYE8DvgfT8Kzvcm5Nxa/Ok010IoAHYF0LdOeB0rkDtzmll9Y0qFjc+6EkgzIZJOXJIOOeO+aHXW4pH6Y5HMk7Dq9ygHn1P8ASB/apAJM3XHQj3oW6NT0yWKWHV7pBAp6VBUI/Ynr4+LPnzmtY2p7V9IljNvfExyLIFLDAznucDjOT2GPpXN32y+NsJGUOcg4Jwi/+RJ74r1ZajZ29xJLHcQvdAZX3ecn0BxnioIF0JJxXyZ2vp2pWl/aRXFtPHJHJ90qe9V9b0W11OLLKI5h9117iuYNg7+TQtVi1COYzxRhi8fUwVlYEHvxnz+FdDbD3ppu5IFi+1Wy3nQJRErYZ4z2bpPIxwCPH41mA3uYOhQ3AGoWl3ps3urxfhzhJB2NR9ZBHNaVeWdvdwGG4jV1b5Uhbl0WfR83EHVLa5yR5Wrbq7lRzIYpyD34q1HOp85oJb3UUg6kOT6eanWcg1PBlrIjvubBlbPhaBray3NmUibpPeiWr3iXNzcqh4Q4r5pbCK0aRzgDNCE9kTRR0DFcx3FvOYyc4NNGs68mlbFudR94Ekji6U9S54GBQO5lWW5dwByeKXfbEl5J7LrmWzjBeK4TL4OVUnBxz5JHrW/QEqFs1MG3bcW9ze3FxO8bXbXBZGY9fXkDAxnjHf8AEUQi2zfHb6sQ3U/Ljp78c0vbB0mXWN22kFyoMMEpkKAfD3zk+tdFmzhWNUKLgDtiuc18QYdiUVdTmHWtO1CzMmYmCEEAgHvS6j6rIfdW6mEE8DgM5HqfNdQ6npFlcI6NChRh90jile82pYnkQqoXkYWqrqXXgi4QdMj8k1MRsluZCTey3BJ7DqI/UVPcR3KRmCKRwreFiIY+mW7nHzrUm2vazpJmMDLf/oP61F/hXrUoY2Y9gwHNcNQd3Us2nTb3ELbEU8bGyYmVOD1N2B+vy5p425uX+HW97DHHi4I97FIn+pbMuSHjx905yCOxycg5qPUNrXenaVcy26qrpGSUxyy+fpQ/ZIje/SSOJTlsPER1A48AjnnPY+vetwS3JEBy41UUDOxtlawuvbU0zWgU/wA1bLIxUEL1YwcZ8ZBqDdOrWaWUlt8MrsCCPApdt72LTdCtNK04CG2t4ggA475J/UmiWzpdCnEz6jNC8+cBJCO1ZIpyttECasQ3GKscEaxAiMKfUV4YgVo9htrQ5G960omViSqCTCgeBxRu10bSrYD3Njbr8+kE1uNKx7MyOqX0IlX1t7qSWZcgSHJqlfymKyVA+Ax7Vat9QTUNIjkyC5FD9bjBgjOTk0GqkKQYYWBYESl1YAqDeZ977ONYt5JCg6VII/3Af3q3aWzNae86hlT2NCN+ySf4D1eGNcOYgwPphgT+mam7E4CiJmfsq06G11h/drnoBy57kmtNuS3V9BzSd7KLQyxXV8w+AN0KfXzTbeyJGMkgZ9aobq4wxC2qUpkcg1RmUnPGauPdRtwMfhVO8BkTpWcxKe5AyaxBMMCgwVppDpLJj4PfsoJ+XGfpRSGPBxgVHDBFFEkSAIgGFFTpbQcB2Ygf09ZxWmMtcq6gCe4bZLwTo6howhU/P1FZh7M9Bkst5fEC1sGcvzn4lbK/pWxWEA92wjAC9JUAeKUduRpDqV4yRBZA7B2PrnH7CmaA7LMS6h6bbL+5dU1l9QWG1QR2o++3k/Sq9tJKAXy5fHc0ThjaSctMy9HjmrgW1X7oX96FHx6mRx2bJmd3u6NyaffSCKC76Fb4SpNSR+1/c8Mo65b3qwBliafZktmGSiH8KHXdhYyKS1uhP0qd23mpPiDccQ7sS+lukZHY9KHCii+6r42jQjGVNLXstBe3uGP9MmP0opvZszQr6VpqB2Zhg9COm27eJ9HLTdOXGQKzjdJur671nS1kkWIRNEFBx47/AI5opZbigtrZI2uCCo7ZoRrdx9uea4s34uGVHYd88gn9RQufHsUOsafjcgbKyNRNcSrslG0/2e28q8Szl2BYec4/YVmm+9walJdzxxXtw7wnDi1h6gmTgDqJAznwK17bUCT7N06Jj1qkRXPqQSKH6npdqYDELePpHjp4qj8kH1CsJC2a5mCaDr+40uveLqt6IGkEfXNCrJk8gcHvW0bba9vNPSSdkZwOWXsTVH/C0EjALDGgBzgIMflTXpViLPTiDwerNYOCzWOBDVZdoXszPd/a/qOgIyxNArkfAXBP6CkSw3ruK+nydTtoyG6cMjIufrjFa7vfbx1N4plVCVww6lzyO340uaLs+CHVBdGHpkwR1KcjB7jB47E+PNGYkCmmg+TJuQFasdxk9l2s6vcSpb6nEjo4/lzRtkVY0zSxc6tqgEwRZLtkx1YIwxohtPQbTSLhY9OVo4S3UIy2Qh+Wewqhcq5v1eEqM6rLK+O2FORn8qMV2x4xfJuLW06ajMRdCif6jIuyIyg6bydeP/vXhtkyKPh1Cf8AOov45fxsql8g+a+3O5LxMe7YNTrwIfU8p5n+58k2Xe4IXUpKibaOqKMLfkj5rU9puS/dyGA7UcsNTmmtg7Ac1H+NjPoSTqMg9mKnsiYnT7knzMaKbwfqvEHyoF7IHZtOuOf+81GtzMv8STq7Ac0ky8gmOsYoiImpabqbyvNBIFU9gRRvY4uIrC6t78L1D4kYnuCRkfpTbBqm2GtVhkdA4GDVKWDQ58/Z7xRnwGqrY2ZdtzsOoGLLvqV9mSn+Ay24bPuLuWMN4Izn+9WL0gseeBQvaTxQJq1mgK+7vnxnzkDmvmtXgiQ5I+tAZm2LH+lIf9ya1uka9WCPDOe9FrshIVUjluTSSNTsLKFrp7xUfnBVh1Z9BVOD2h2NzbgO7l046mGMgeayxZLXmGeIlvjHu8dEtElYYU/CT6VSEKs4deCfI80I0zcdtrdk0VrcB4xwwxjOaKaZIcBGOfrTBnsgQEJW4n7hBboWNnPc9OTFGSAO5PYD8yKBC3eztxFIcyO3Uxz5PJq5uqZLTT7dXBPXOrMB5C8/viqmpXEl4Y5o1ClsEZ8UUmMkrcWZtSER67PH6nt/vDJz9atQw20idTuF+VfdM0b3wEt7eKFPgGjsNpt21h6ZLhD8ya7x5CeT/cXeTGBwIuypFGcwtkYq7YXVwIFVMdNGtJ0bQNQlmdLtAqjnDUyaJsrR5tPjl9+7dXOVbirpiyXdyrZMZ9TBNC3NZ7atZLayj1G6aRy7SmFFUfTLUL3B7QNTV3uI4b5yFyEaOE5+XDZpGt9caZWjkcCaLhucda+v1r9PqcMsXT7xli8HPMZ/4q3xjRcKiGod/T32nvcXKCKTqKmOKJpOg+M8dvX0ottb2g6DEizXUDI68P0sWTPyb/msl1KZrbUGweiOXhgrfr/ehD38jxNDIf5jz4YgYyBgVO1WHIkMig8zfLfdkKbglvYiYrO/T3sfWMZI4Jx8gK87q3DJqSx29j1EsuRjs2Tj60D3LodzP7ItOv7IM17Zq8/SBy0Tdx68AK2PrSV7PtzyQXsZmHWC+ASOc+MUl1eLdbJ6MZ6TIqOFP6jlc6RqKwL9ss53kz1K0RDDp9MeKCPpVrJOlxJqBheMkNE64x8ua2AajavYJIY+tWUHHkH1pavNxaM16IpR1sCMB4gcZ480NhKkdx1iy4yPksU9DnewuzJHde7HIBCMev8AL/3zWjbI1eS5SRp3JIYHJ4J54OKmsW02WETEA/Dwen4QPl6Ujb/3FZ7duRFZABrjJYqe+OOaLVLcBTcV6rJjo1xHfdG69HN8LS+u/dhYiFKoWAYnzjtxXm7vIb/SIxpmoW0kgHH80J++KwKXVpbzUWuJGyuQMfIURt77M0SZ+EvyPWnafGogbTLkBN9zXdunXYb2SHUzIIx9znqVvofNRbwTUHsWNr19RbwaAbT3i7SJEZOtDnKv2wOKcJmvL2H3ln0yQuMjjkfI1ouIM1k0DAM+A4VG0XBm1RrFlbyObh0V1HUM96ebTXNYg0GM2l/IjBeATSe1lqhjCkEDzgVKiakkSxhgAvirHxpwrAzP5PyVM59ur1op45g7Kx+FsH8q/fbSD3yG8Y5B80KZve28kTnDr2z3qBZyVU8jjqOfXsaHqN/IRCGoXJltjkHqjOB/t8UMd/8ANxv2zhsZ81OzEuy9ywIzVCVvgB7dLcmuEjI3udYaJIg25YRxkFVtYwPQ/CKwf2haFdbU3Ct/aL/064mMkbD/ALT+UPoOeK0b2U7gTU9pW0XWDNar7mRfIx2P4jFH9ZsrPUtOnsr2ESQTKVZfr5Fee87YcxDDj3HXgGXECp57EWNA3lp89lDazADqTLc4wMdvlVcX2nz6mZ/dxhQQqgDgfj9KQtx7G3BoM3v9Kf8AiNqT8Ij4dR6FfP4UHFxuhZ0sjpl975uET3DdXPbxWo0aObxNITXNiFZVIM2XU9xRWmjzx2YSEqhPxNxWR6rqp1DV7eeR+pyhYEk/Pn/30ph07aut/wALuNY3cpsNLtYnleDrHvZ+OFwM9OTgZ+fFZ5DKW1zrKhMu3wqOFGOw+VNNHgGIfZivV6g5SOKEJWkuF5bn0ohps+LiLBJw7nOfRaDW74x1YH41Y0+cxzEj+kSEc/ICi5krxh25dFbuSQnAhgY9/JraPZZroiSSCQCTKDhj5xmsD0eTCzM3aRgp+g5/4p92XfGO4tQrfFc3KqMH+ktj9ga17xlR/MqafubyNYtWTpa1U59CKH3UdlNIZA00WfC9qW7SzdL1lEsmM8DqqTVPtSTL7mcxgADAGcmvMLqswBY+oS2mxWB9zmaSVeqO4j6Rk4I9PlUKEe8kXOByR+IP9xX6Zslu3S+M+Ok+DVdH/nxnHPYj5g16KAs3PMuRvwpx4xyaqSgZlQ+eRU0ZLL3x3HAqKdf8xg55GP0qakE8QnsbcdztzVluYyWib4J488MPX61vml69Yajaxzwyr0svY8YrmQcsrj05oxpGs3+m/wDx5SYz3Unil+r0QzncODD9DrvCNj9f6nRU0gALIykeOa9WV6AxJRYwo5cmsQt996pEuB0kY7MOBVPVNz6xqi+6muWWJu6L8Kn8u9D6f8dkDAwzN+RwlaEZfbNvaPU4o9A0qTqtTKDPKO0hB7D5D18ms6tsjU1OeTKRX3Uhi4txnJByfzryg6btX7/zc5H1pwMYQUIkdy72ZaT4TnH9R5r1E5CzMOAFYftUZP8AqYGSGP7146v5cqjt1YJ/LP7VMkkQhaSlIkiBILn96dNm3Cya7pkecrGWmPnCrwv5kk0gwMHlUkkD5eBTxsND7+S9fhpCEj+SL/ya2xJuMkH6m4WjK123T9a8XvSZnDc4IxUOizB1E5YAMg81TvLlX1L4JA3ODg15TU4/GHU/f/Yxx/JlP8T/2Q==" alt="">
                    </div>
                    <ul>
                        <li><a href="">Staff</a></li>
                        <li><a href="">Setting</a></li>
                        <li><a href="">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
         @yield('content')
        <div class="allSideBar">
            <ul>
                <li>
                    <a href="/admin">
                        <i>
                            <svg-icon :icon="'#dashboard'"></svg-icon>
                        </i>
                        Dashboard
                    </a>
                </li>
                <li>
                    <a href="/admin/product">
                        <i>
                            <svg-icon :icon="'#basket'"></svg-icon>
                        </i>
                        Product
                    </a>
                </li>
                <li>
                    <a href="/admin/category">
                        <i>
                            <svg-icon :icon="'#category'"></svg-icon>
                        </i>
                        Category
                    </a>
                </li>
                <li>
                    <a href="/admin/order">
                        <i>
                            <svg-icon :icon="'#order'"></svg-icon>
                        </i>
                        Orders
                    </a>
                </li>
                <li>
                    <a href="/admin/user">
                        <i>
                            <svg-icon :icon="'#customer'"></svg-icon>
                        </i>
                        کاربران
                    </a>
                </li>
                <li>
                    <a href="/admin/role">
                        <i>
                            <svg-icon :icon="'#allow'"></svg-icon>
                        </i>
                        مقام ها
                    </a>
                </li>
                <li>
                    <a href="/admin/permission">
                        <i>
                            <svg-icon :icon="'#permission'"></svg-icon>
                        </i>
                        دسترسی ها
                    </a>
                </li>
                <li>
                    <a href="/admin/coupon">
                        <i>
                            <svg-icon :icon="'#coupon'"></svg-icon>
                        </i>
                        Coupon
                    </a>
                </li>
                <li>
                    <a href="/admin/setting">
                        <i>
                            <svg-icon :icon="'#setting'"></svg-icon>
                        </i>
                        Setting
                    </a>
                </li>
            </ul>
            <div class="logout">
                <a href="/login">
                    <i>
                        <svg-icon :icon="'#door'"></svg-icon>
                    </i>
                    logout
                </a>
            </div>
        </div>
    </div>
</div>
<script src="/js/app.js"></script>
</body>
</html>
