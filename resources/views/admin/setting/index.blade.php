@extends('admin.master')
@section('content')
    <div class="allSettingPanel">
        <div class="settingItem">
            <div class="settingItemIcon">
                <i>
                    <svg-icon :icon="'#staff'"></svg-icon>
                </i>
            </div>
            <div class="settingItemDetail">
                <h3>Staff Members</h3>
                <span>Manage your employees and their permission</span>
            </div>
        </div>
        <div class="settingItem">
            <div class="settingItemIcon">
                <i>
                    <svg-icon :icon="'#setting'"></svg-icon>
                </i>
            </div>
            <div class="settingItemDetail">
                <h3>Site Settings</h3>
                <span>View and update your site settings</span>
            </div>
        </div>
        <div class="settingItem">
            <div class="settingItemIcon">
                <i>
                    <svg-icon :icon="'#basket'"></svg-icon>
                </i>
            </div>
            <div class="settingItemDetail">
                <h3>Add Products</h3>
                <span>Add products from here</span>
            </div>
        </div>
        <div class="settingItem">
            <div class="settingItemIcon">
                <i>
                    <svg-icon :icon="'#category'"></svg-icon>
                </i>
            </div>
            <div class="settingItemDetail">
                <h3>Add Categories</h3>
                <span>Add product categories from here</span>
            </div>
        </div>
        <div class="settingItem">
            <div class="settingItemIcon">
                <i>
                    <svg-icon :icon="'#addUser'"></svg-icon>
                </i>
            </div>
            <div class="settingItemDetail">
                <h3>Add Staff Members</h3>
                <span>Add your staff members from here</span>
            </div>
        </div>
        <div class="settingItem">
            <div class="settingItemIcon">
                <i>
                    <svg-icon :icon="'#coupon'"></svg-icon>
                </i>
            </div>
            <div class="settingItemDetail">
                <h3>Add Coupons</h3>
                <span>Add coupons from here</span>
            </div>
        </div>
    </div>
@endsection
