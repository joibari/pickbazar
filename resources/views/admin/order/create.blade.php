@extends('admin.master')
@section('content')
    <div class="allOrderCreate">
        <div class="allOrderCreateTitle">
            <span>Order</span>
            <category-panel></category-panel>
            <input type="text" placeholder="Ex: Search By Name">
        </div>
        <order-table></order-table>
    </div>
@endsection
