@extends('admin.master')
@section('content')
    <div class="allCategoryCreate">
        <div class="allCategoryCreateTitle">
            <span>Category</span>
            <category-panel></category-panel>
            <input type="text" placeholder="Ex: Search By Name">
            <button>
                <i>
                    +
                </i>
                Add Category
            </button>
        </div>
        <table-panel></table-panel>
    </div>
@endsection
