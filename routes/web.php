<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'] , function (){
    Route::get('/',  [App\Http\Controllers\Admin\PanelController::class, 'index']);
    Route::get('/product',  [App\Http\Controllers\Admin\ProductController::class, 'index']);
    Route::get('/category',  [App\Http\Controllers\Admin\CategoryController::class, 'index']);
    Route::get('/order',  [App\Http\Controllers\Admin\OrderController::class, 'index']);
    Route::get('/customer',  [App\Http\Controllers\Admin\CustomerController::class, 'index']);
    Route::get('/coupon',  [App\Http\Controllers\Admin\CouponController::class, 'index']);
    Route::get('/setting',  [App\Http\Controllers\Admin\SettingController::class, 'index']);
    Route::get('/user',  [App\Http\Controllers\Admin\UserController::class, 'index']);
    Route::post('/user',  [App\Http\Controllers\Admin\UserController::class, 'store']);
    Route::get('/get-user',  [App\Http\Controllers\Admin\UserController::class, 'getUser']);
    Route::get('/user/{user}/edit',  [App\Http\Controllers\Admin\UserController::class, 'edit']);
    Route::put('/user/{user}',  [App\Http\Controllers\Admin\UserController::class, 'update']);
    Route::delete('/user/{user}',  [App\Http\Controllers\Admin\UserController::class, 'remove']);
    Route::post('/gallery',  [App\Http\Controllers\Admin\GalleryController::class, 'store']);
    Route::get('/gallery/get-gallery',  [App\Http\Controllers\Admin\GalleryController::class, 'getGallery']);
    Route::post('/search-user',  [App\Http\Controllers\Admin\SearchController::class, 'searchUser']);
    Route::post('/search-role',  [App\Http\Controllers\Admin\SearchController::class, 'searchRole']);
    Route::get('/role',  [App\Http\Controllers\Admin\RoleController::class, 'index']);
    Route::post('/role',  [App\Http\Controllers\Admin\RoleController::class, 'store']);
    Route::get('/get-role',  [App\Http\Controllers\Admin\RoleController::class, 'getRole']);
    Route::get('/role/{role}/edit',  [App\Http\Controllers\Admin\RoleController::class, 'edit']);
    Route::put('/role/{role}',  [App\Http\Controllers\Admin\RoleController::class, 'update']);
    Route::delete('/role/{role}',  [App\Http\Controllers\Admin\RoleController::class, 'remove']);
    Route::get('/permission',  [App\Http\Controllers\Admin\RoleController::class, 'permission']);
    Route::post('/permission',  [App\Http\Controllers\Admin\RoleController::class, 'changePermission']);
});

Route::get('/login',  [App\Http\Controllers\Admin\UserController::class, 'login']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
